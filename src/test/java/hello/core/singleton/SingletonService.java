package hello.core.singleton;

public class SingletonService {

    // static 영역에 객체를 1개 올린다
    private static final SingletonService instance = new SingletonService();

    //public으로 열어 두어서 객체 인스턴스가 필요할 때 이 static 메소드를 조회하도록 허용한다
    public static SingletonService getInstance() {
        return instance;
    }

    //생성자를 private화 해서 외부에서 new 키워드로 새로운 객체 생성 시도를 막는다
    private SingletonService() {
    }

}
