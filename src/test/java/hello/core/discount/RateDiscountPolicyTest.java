package hello.core.discount;

import hello.core.member.Grade;
import hello.core.member.Member;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class RateDiscountPolicyTest {

    RateDiscountPolicy discountPolicy = new RateDiscountPolicy();

    @Test
    @DisplayName("VIP는 20%할인이 적용되어야 한다")
    void vipSaleTest() {
        Member memberA = new Member(1L, "memberA", Grade.VIP);
        int discountPrice = discountPolicy.discount(memberA, 10000);
        assertThat(discountPrice).isEqualTo(2000);
    }

    @Test
    @DisplayName("일반회원은 할인이 없다")
    void normalSaleTest() {
        Member memberA = new Member(1L, "memberA", Grade.BASIC);
        int discountPrice = discountPolicy.discount(memberA, 20000);
        assertThat(discountPrice).isEqualTo(0);
    }
}